/**
 * Gedare Bloom
 *
 * Test code in C
 */

#include <stdlib.h>
#include <stdio.h>

#define DIM1 2
#define DIM2 3
#define DIM3 DIM2
#define DIM4 2

#define SCENARIO (1)

void mm(long **A, long **B, long **X, int ar, int ac, int bc)
{
  int i, j, k;

  for (i = 0; i < ar; i++)
    for (j = 0; j < bc; j++)
      for (k = 0; k < ac; k++)
         X[i][j] += A[i][k] * B[k][j];
}


int main(int argc, char *argv[])
{
  long **A, **B, **X;
  int i,j;
  long long sum = 0;

  A = malloc(DIM1*sizeof(long*));
  for (i = 0; i < DIM1; i++)
    A[i] = malloc(DIM2*sizeof(long));

  B = malloc(DIM3*sizeof(long*));
  for (i = 0; i < DIM3; i++)
    B[i] = malloc(DIM4*sizeof(long));

  X = malloc(DIM1*sizeof(long*));
  for (i = 0; i < DIM4; i++)
    X[i] = malloc(DIM4*sizeof(long));

  for (i = 0; i < DIM1; i++)
    for (j = 0; j < DIM2; j++)
#if SCENARIO == 1
      A[i][j] = i+j;
#else
      A[i][j] = -(i+j);
#endif

  for (i = 0; i < DIM3; i++)
    for (j = 0; j < DIM4; j++)
      B[i][j] = i|j;

  for (i = 0; i < DIM1; i++)
    for (j = 0; j < DIM4; j++)
      X[i][j] = 0;

  mm(A, B, X, DIM1, DIM2, DIM4);

  for (i = 0; i < DIM1; i++)
    for (j = 0; j < DIM4; j++)
      sum += X[i][j];

  for (i = 0; i < DIM1; i++) free(A[i]);
  for (i = 0; i < DIM3; i++) free(B[i]);
  for (i = 0; i < DIM1; i++) free(X[i]);
  free(A); free(B); free(X);

  printf("%lld\n", sum);

  return 0;
}

